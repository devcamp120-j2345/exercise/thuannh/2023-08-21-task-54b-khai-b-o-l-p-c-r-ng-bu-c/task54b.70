import java.util.Date;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer("John Smith", true, "Gold");
        Customer customer2 = new Customer("Mary Johnson", false, null);
        System.out.println(customer1.toString());
        System.out.println(customer2.toString());

        Visit visit1 = new Visit(new Date(), 50.0, 100.0, customer1);
        Visit visit2 = new Visit(new Date(), 75.0, 50.0, customer2);
        System.out.println(visit1.toString());
        System.out.println(visit2.toString());

        double totalExpense = visit1.getTotalExpense() + visit2.getTotalExpense();
        System.out.println("Tổng chi phí: " + totalExpense);
    }
}
